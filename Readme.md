This is a minimalist project that can be used as a template to program the NUCLEO F401RE using
the HAL (hardware abstraction library) of STMicroElectronics.


## Compiling

### For Linux

You need to install the ARM version of the GCC compiler and cmake
```
sudo apt install gcc-arm-none-eabi cmake
```
Then you can compile this project by typing the following commands (from the root of the project)
```
mkdir build
cd build
cmake ..
make
```
If everything went well, you should find a file named `stm32_hal.bin` that you can copy on the
mounted NUCLEO. 
The program prints `hello world` in the USB serial device of the NUCLEO and makes the LED2 blink.

### For Windows

You need to install the ARM version of the GCC compiler, cmake and Ninja. During installation don't forget to check the option "Add to path".
- [CMake](https://cmake.org/download/)
- [gcc](https://developer.arm.com/downloads/-/gnu-rm)
- [Ninja](https://github.com/ninja-build/ninja/releases) -> This one need to be manually added to the path.

Not sure about this one, but if you encouter an error during CMake generation regarding `windres` you should install [msys](https://www.msys2.org/)

Then you can compile this project by typing the following commands (from the root of the project)
```
mkdir build
cd build
cmake .. -G Ninja
cmake --build .
```
If everything went well, you should find a file named `stm32_hal.bin` that you can copy on the
mounted NUCLEO. 
The program prints `hello world` in the USB serial device of the NUCLEO and makes the LED2 blink.
