#include "driver_config.h"

int main(void)
{
	HAL_Init();
	SystemClock_Config();
	MX_GPIO_Init();
	MX_USART2_UART_Init();

	{
		uint8_t buf[] = "hello world\n";
		HAL_UART_Transmit(&huart2, buf, 12, 500);
	}

	while(1)
	{
		HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
		HAL_Delay(500);
	}
}

